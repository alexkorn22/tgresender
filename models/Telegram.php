<?php


namespace app\models;


use yii\base\Model;

class Telegram extends Model
{

    public $token;
    protected $api = "https://api.telegram.org/bot";
    public $idChat;

    public function init(){
        parent::init();
        $this->api .= $this->token;
    }

    public function rules()
    {
        return [
            [['token', 'idChat'], 'require']
        ];
    }

    protected function getUrl($command, array $params= []) {
        $res = $this->api . '/' . $command;
        if (!empty($params)) {
            $res .= '?' . http_build_query($params);
        }
        return $res;
    }

    protected function getResult($command, array $params= []) {
        try {
            return file_get_contents($this->getUrl($command, $params));
        } catch (\Exception $e) {
            return '';
        }
    }

    public function sendMessage($message) {
        if (!$this->token || !$this->idChat) {
            return false;
        }

        $dataGet = array(
            'chat_id' => $this->idChat,
            'text' => $message,
            'parse_mode' => 'HTML',
        );

        return $this->getResult('sendMessage', $dataGet);
    }

    public function getUpdates() {
        return $this->getResult('getUpdates');
    }

}